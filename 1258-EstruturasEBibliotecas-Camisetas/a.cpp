#include <iostream>
#include <sstream>
#include <vector>
#include <tuple>
#include <bits/stdc++.h>

bool sort(const std::tuple< std::string, std::string, std::string >& a,
    const std::tuple< std::string, std::string, std::string >& b) {

    if(std::get<0>(a) < std::get<0>(b))
      return true;
    else if(std::get<0>(a) == std::get<0>(b)) {
      if(std::get<1>(a) > std::get<1>(b))
        return true;
      else if(std::get<1>(a) == std::get<1>(b))
          return(std::get<2>(a) < std::get<2>(b));
    }
    return false;
}

bool sortbyfirst(const std::tuple< std::string, std::string, std::string >& a,
    const std::tuple< std::string, std::string, std::string >& b) {
    return (std::get<0>(a) < std::get<0>(b));
}

bool sortbysec(const std::tuple< std::string, std::string, std::string >& a,
    const std::tuple< std::string, std::string, std::string >& b) {
    return (std::get<1>(a) > std::get<1>(b));
}

bool sortbythird(const std::tuple< std::string, std::string, std::string >& a,
    const std::tuple< std::string, std::string, std::string >& b) {
    return (std::get<2>(a) < std::get<2>(b));
}

int main() {
  int n;

  while(true) {
    std::cin >> n;
    if(n == 0)
      break;

    std::string name;
    std::string color;
    std::string size;

    std::vector <std::tuple< std::string, std::string, std::string > > orders;

    while(n > 0) {
      n--;
      std::getline(std::cin, name);
      std::getline(std::cin, name);
      std::cin >> color >> size;
      orders.push_back(std::make_tuple(color, size, name));
    }

    //sort(orders.begin(), orders.end(), sortbythird);
    //sort(orders.begin(), orders.end(), sortbysec);
    //sort(orders.begin(), orders.end(), sortbyfirst);
    sort(orders.begin(), orders.end(), sort);


    for(auto tuple : orders) {
      std::cout << std::get<0>(tuple) << " " <<  std::get<1>(tuple)
        << " " << std::get<2>(tuple) << "\n";
    }
    std::cout << "\n";

  }
}
