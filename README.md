Solutions of URI Online Judge problems:

```
.
├── 1119-EstruturasEBibliotecas-AFilaDeDesempregados
│   ├── a.cpp
│   ├── a.py
│   ├── in
│   └── out
├── 1258-EstruturasEBibliotecas-Camisetas
│   ├── a
│   ├── a.cpp
│   ├── in
│   └── out
└── README.md
```
